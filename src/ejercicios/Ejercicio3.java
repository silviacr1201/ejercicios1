package ejercicios;
/*
 * Ejercicio 3
 * Hacer la matriz con los ceros abajo
 */
public class Ejercicio3 {
	

	public static void main(String[] args) {
		
	//Declaramos variables
	
			int x,y;
			x=6;
			y=6;
			
			//Ahora declaramos la matriz, en este caso al ser una matriz que incluye * usamos char en lugar de int

	        char[][] matriz2 = new char[x][y];
	        
			//Declaramos los elementos de la matriz en base a la condición del ejercicio		
			for (int i = 0; i < x; i++) {
	            for (int j = 0; j < y; j++) {

	                if (i<x-1)
	                    matriz2[i][j] = '*';
	              
	                else
	                    matriz2[i][j] = '0';

	            }
	        }

	        //Finalmente imprimimos por consola la matriz
	        for (int i = 0; i < x; i++) {
	            int j = 0;
	            while (j < y) {
	                System.out.print(matriz2[i][j]);
	                j++;
	            }
	          //Incluimos un salto de linea para que se muestren los elementos en forma de matriz
	            if (j == x)
	                System.out.print("\n");
	        }
	}

}
	
