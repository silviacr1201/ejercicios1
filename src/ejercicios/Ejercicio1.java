package ejercicios;
/*
 * Ejercicio 1
 * Hacer una matriz de *s
 */
public class Ejercicio1 {
public static void main(String[] args) {
		
	//Primero declaramos las variables que necesitamos, en este caso dos para definir las dimensiones de la matriz
		
		int x,y;
        x = 3;
        y = 2;
        
     //Ahora declaramos la matriz, en este caso al ser una matriz de * usamos char en lugar de int

        char[][] matriz1 = new char[x][y];
        
     //Declaramos los elementos que van a componer la matriz
              
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++)
                matriz1[i][j] = '*';}
        
        //Ahora pedimos que se muestre la matriz por consola
        for (int i = 0; i < x; i++)
        {
            int j = 0;
            while (j < y)
            {
                System.out.print(matriz1[i][j]);
                j++;
            }
          //Incluimos un salto de linea para que se muestren los elementos en forma de matriz
            if (j == y)
                System.out.print("\n");
            
        }
    }
}

