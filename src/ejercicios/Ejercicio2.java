package ejercicios;
/*
 * Ejercicio 2
 * Hacer una matriz que ponga 0s de la diagonal hacia abajo 
 * y 1s de la diagonal hacia arriba
 */
public class Ejercicio2 {

	public static void main(String[] args) {
		
		//Declaramos variables
		
		int x,y;
		x=6;
		y=6;
		
		//Ahora declaramos la matriz, en este caso al ser una matriz que incluye * usamos char en lugar de int

        char[][] matriz2 = new char[x][y];
        
		//Declaramos los elementos de la matriz en base a la condición del ejercicio		
		for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {

                if (i==j)
                    matriz2[i][j] = '*';
                else if (i > j)
                    matriz2[i][j] = '0';
                else
                    matriz2[i][j] = '1';

            }
        }

        //Finalmente imprimimos por consola la matriz
        for (int i = 0; i < x; i++) {
            int j = 0;
            while (j < y) {
                System.out.print(matriz2[i][j]);
                j++;
            }
          //Incluimos un salto de linea para que se muestren los elementos en forma de matriz
            if (j == x)
                System.out.print("\n");
        }


	}

}
