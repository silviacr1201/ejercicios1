package calculadora;


import calculadora.Calculadora;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.io.IOException;

public class Calculadora1 {
	public static void main(String[] args) throws IOException {

		//Declaramos una variable que lea texto introducido por consola
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        //Declaramos una instrucci�n para pedir los n�meros que se usaran en la operaci�n
        System.out.println("Introducir un n�mero: ");
        int num1 = Integer.parseInt(reader.readLine());

        System.out.println("Introducir otro n�mero: ");
        int num2 = Integer.parseInt(reader.readLine());

       //asignamos los n�meros introducidos por consola a la calculadora

        Calculadora ejemplo1 = new Calculadora(num1,num2);
        
       //Definimos que operaci�n se va a realizar 

        System.out.println("Que operaci�n se realizara a los n�meros:");
        System.out.println("suma ; resta ; multiplicacion ; division ");
       //asignamos la operacion que se pide a una variable
        Scanner in = new Scanner(System.in);
	    
        String s = in.nextLine();
        System.out.println("Usted eligio: "+s);      
       //Decidimos que operaci�n se va a realizar en base a lo que se ha pedido 
        switch(s) {
            case "suma":
                System.out.println("El resultado de la suma es  ");
                System.out.println (ejemplo1.Suma());
                break;
            case "resta":
                System.out.println("El resultado de la resta es ");
                System.out.println(ejemplo1.Resta());
                break;
            case "multiplicacion":
                System.out.println("El resultado de la multiplicaci�n ");
                System.out.println(ejemplo1.Multiplicacion());
                break;
            case "division":
            	if (num2==0) {
                System.out.println("No es posible realizar la divisi�n, el divisor no puede ser 0 ");
            	}
            	else
            	{
                System.out.println("El resultado de la divis�n es ");
                System.out.println(ejemplo1.Division(num1,num2));
            	}
                break;
            default:
                System.out.println("Selecci�n incorrecta");
        }




    }

}
