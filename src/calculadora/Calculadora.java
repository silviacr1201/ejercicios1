package calculadora;

/*
 * Ejercicio 4
 * Hacer una calculadora que reciba los valores por consola e imprima 
 * el resultado en consola
 */

public class Calculadora {
     
        int x, y;

        public Calculadora (){

        this.x = 0;
        this.y = 0;
    }

    public Calculadora (int X, int Y){
        this.x = X;
        this.y = Y;
    }
    public int Suma(){return this.x + this.y; }

    public int Resta(){return this.x - this.y; }

    public int Multiplicacion (){return this.x * this.y; }

    public float Division (int X, int Y){return X / (float)Y; }
}


